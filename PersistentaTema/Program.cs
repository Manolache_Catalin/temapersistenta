﻿using System;

namespace PersistentaTema
{
    class Program
    {
        static int persistenta(int numar)
        {
            //variabile de control
            int count = 0;
            //
            while (numar >= 9)
            {
                //variabile ajutatoare
                int temp = numar;
                int v = 1;
                //loop pentru spargerea numarului curent
                while (temp != 0)
                {
                    v *= temp % 10;
                    temp = temp / 10;
                }
                count++;
                numar = v;
            }
            //returnez contorul
            return count;
        }
        static void Main(string[] args)
        {
            int numar = int.Parse(Console.ReadLine());
            Console.WriteLine(persistenta(numar));
        }
    }
}
